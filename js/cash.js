var subTotalGlobal = 0;
var totalComisionGlobal = 0;
var totalPagarGlobal = 0;




// FUNCIONES
function change(){
    let slcMonedaOrigen = document.getElementById('slcMonedaOrigen');
    let slcMonedaDestino = document.getElementById('slcMonedaDestino');
    let divDestino = document.getElementById('divDestino');

    seleccion = slcMonedaOrigen.value;
    opciones = "";

    if(seleccion != '1')
        opciones += "<option value='1'>Pesos Mexicanos</option>";
    if(seleccion != '2')
        opciones += "<option value='2'>Dolar Estado Unidense</option>";
    if(seleccion != '3')
        opciones += "<option value='3'>Dolar Canadience</option>";
    if(seleccion != '4')
        opciones += "<option value='4'>Euro</option>";

    slcMonedaDestino.innerHTML = opciones;
}

function calcular(){
    let txtCantidad = document.getElementById('txtCantidad');
    let slcMonedaOrigen = document.getElementById('slcMonedaOrigen');
    let slcMonedaDestino = document.getElementById('slcMonedaDestino');
    let txtSubtotal = document.getElementById('txtSubtotal');
    let txtTotalComision = document.getElementById('txtTotalComision');
    let txtTotalPagar = document.getElementById('txtTotalPagar');

    if(txtCantidad.value != ''){
        cantidad = txtCantidad.value;
        subTotal = cantidad;
        switch(slcMonedaOrigen.value){
            case '1':{
                subTotal /= 19.85;
                break;
            }
            case '3':{
                subTotal /= 1.35;
                break;
            }
            case '4':{
                subTotal /= 0.99;
                break;
            }
        }
        switch(slcMonedaDestino.value){
            case '1':{
                subTotal *= 19.85;
                break;
            }
            case '3':{
                subTotal *= 1.35;
                break;
            }
            case '4':{
                subTotal *= 0.99;
                break;
            }
        }
        comision = subTotal*0.03;

        txtSubtotal.value = subTotal.toFixed(4);
        txtTotalComision.value = comision.toFixed(4);
        txtTotalPagar.value = (subTotal+comision).toFixed(4);
    }
}

function registrar(){
    let txtCantidad = document.getElementById('txtCantidad');
    let txtSubtotal = document.getElementById('txtSubtotal');
    let txtTotalComision = document.getElementById('txtTotalComision');
    let txtTotalPagar = document.getElementById('txtTotalPagar');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    if(txtSubtotal.value!='' && txtTotalComision!='' && txtTotalPagar!=''){
        subTotalGlobal = parseFloat(txtSubtotal.value);
        totalComisionGlobal += parseFloat(txtTotalComision.value);
        totalPagarGlobal += parseFloat(txtTotalPagar.value);

        lblSubtotal.innerHTML = "$" + subTotalGlobal.toFixed(2);
        lblTotalComision.innerHTML = "$" + totalComisionGlobal.toFixed(2);
        lblTotalPagar.innerHTML = "$" + totalPagarGlobal.toFixed(2);

        txtSubtotal.value = '';
        txtTotalComision.value = '';
        txtTotalPagar.value = '';
        txtCantidad.value = '';
    }
}

function borrar(){
    subTotalGlobal = 0.0;
    totalComisionGlobal = 0.0;
    totalPagarGlobal = 0.0;

    let txtCantidad = document.getElementById('txtCantidad');
    let txtSubtotal = document.getElementById('txtSubtotal');
    let txtTotalComision = document.getElementById('txtTotalComision');
    let txtTotalPagar = document.getElementById('txtTotalPagar');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    txtSubtotal.value = '';
    txtTotalComision.value = '';
    txtTotalPagar.value = '';
    txtCantidad.value = '';
    lblSubtotal.innerHTML = '$$$';
    lblTotalComision.innerHTML = '$$$';
    lblTotalPagar.innerHTML = '$$$';
}